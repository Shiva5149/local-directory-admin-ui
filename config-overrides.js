const path = require("path");

module.exports = function override(config) {
  config.resolve.alias = {
    ...config.resolve.alias,
    "@assets": path.resolve(__dirname, "src/assets"),
    "@components": path.resolve(__dirname, "src/components"),
    "@config": path.resolve(__dirname, "src/config"),
    "@containers": path.resolve(__dirname, "src/containers"),
    "@context-api": path.resolve(__dirname, "src/context-api"),
    "@redux": path.resolve(__dirname, "src/redux"),
    "@utility-functions": path.resolve(__dirname, "src/utility-functions"),
    "@contexts": path.resolve(__dirname, "src/contexts"),
  };

  return config;
};
