
# Local Directory Admin Panel

This admin panel is an web application developed using React.js. Here in this application we maintain all the services end to end information.

# Project Setup

1. Clone the Project

   `git clone https://gitlab.com/Shiva5149/local-directory-admin-ui.git`

   2. Install the required packages

   `yarn install`

   *Note*
   - Required node version 14+
   - Install yarn using npm install -g yarn (if yarn not present) --> this is not mandate 
   - you can check yarn version using `yarn -v`


   3. Run the server 

   `yarn start or npm start`



   ## Authors

   - [@Shiva5149 ](https://gitlab.com/Shiva5149)
   
