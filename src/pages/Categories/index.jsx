import React, { useState } from "react";
import {
  Grid,
  Box,
  TextField,
  FormControl,
  FormHelperText,
  Switch,
} from "@mui/material";
import { useForm, Controller } from "react-hook-form";
import Page from "@assets/layout/Page";
import TableWrapper from "@components/Table/TableWrapper";
import ModalComponent from "@components/Modal";
import useFetch from "@hooks/useFetch";
import apiEndpoints from "@utils/apiEndpoints";
import fetchWrapper from "@utils/fetchWrapper";
import useNotistack from "@hooks/useNotistack";

const TABLE_OPTIONS = [
  { id: "edit", caption: "Edit" },
  { id: "delete", caption: "Delete" },
];

const Categories = () => {
  const [data, loading, reFetch] = useFetch(
    apiEndpoints.category.categoriesApi,
    TABLE_OPTIONS
  );
  const [open, setOpen] = useState(false);
  const [category, setCategory] = useState({
    mode: "add",
    name: "",
    description: "",
    category_image: null,
    is_active: true,
  });
  const { showNotification } = useNotistack();
  const {
    handleSubmit,
    control,
    formState: { errors },
    reset,
    watch,
  } = useForm();

  const handleClose = () => {
    setOpen(false);
    handleClear();
  };

  const handleClear = () => {
    setCategory({
      mode: "add",
      name: "",
      description: "",
      category_image: null,
      is_active: true,
    });
    reset();
  };
  const handleEditClear = () => {
    setCategory({
      mode: "edit",
      name: "",
      description: "",
      category_image: null,
      is_active: true,
    });
    reset();
  };

  const handleUpdateCategory = async (data) => {
    try {
      const url = apiEndpoints.category.categoriesApi + `${category.id}/`;
      const response = await fetchWrapper(url, data, "PUT");

      if (response) {
        showNotification("Updated successfully", "success");
        reFetch();
        handleClose();
        handleEditClear();
      } else {
        showNotification("Update failed", "error");
      }
    } catch (error) {
      console.error("Update data error:", error);
    }
  };

  const handleAddCategory = async (data) => {
    try {
      const url = apiEndpoints.category.categoriesApi;
      const response = await fetchWrapper(url, data, "POST");

      if (response) {
        showNotification("Added successfully", "success");
        reFetch();
        handleClose();
        handleClear();
      } else {
        showNotification("Add failed", "error");
      }
    } catch (error) {
      console.error("Add data error:", error);
    }
  };

  const onSubmit = handleSubmit(async (data) => {
    const formData = new FormData();
    formData.append("name", data.name);
    formData.append("description", data.description);
    formData.append("is_active", data.is_active);

    if (category.mode === "edit") {
      await handleUpdateCategory(formData);
    } else {
      formData.append("category_image", category.category_image);
      await handleAddCategory(formData);
    }
  });

  const handleDeleteCategory = async (_rowID, row) => {
    const response = await fetchWrapper(
      apiEndpoints.category.categoriesApi + `${row.id}/`,
      "",
      "DELETE"
    );
    if (response === "success") {
      showNotification(`'${row.name}' Deleted successfully`, "success");
      reFetch();
    }
  };

  const changeStatusToActivate = async (row) => {
    const requestPayload = {
      is_active: !row.is_active,
    };
    const response = await fetchWrapper(
      apiEndpoints.category.categoriesApi + `${row.id}/`,
      requestPayload,
      "PUT"
    );
    if (response) {
      showNotification(`'${response.name}' Activated successfully`, "success");
      reFetch();
    }
  };

  const changeStatusToDeactivate = async (row) => {
    const requestPayload = {
      is_active: !row.is_active,
    };
    const response = await fetchWrapper(
      apiEndpoints.category.categoriesApi + `${row.id}/`,
      requestPayload,
      "PUT"
    );
    if (response) {
      showNotification(`'${row.name}' Deactivated successfully`, response);
      reFetch();
    }
  };

  const columns = [
    {
      Header: "Sl. No",
      accessor: "slno",
    },
    {
      Header: "Name",
      accessor: "name",
    },
    {
      Header: "Description",
      accessor: "description",
    },
    {
      Header: "Image",
      accessor: "category_image",
      type: "image",
    },
    {
      Header: "Status",
      accessor: "is_active",
      type: "toggle",
      handleToggleChange: (rowData, status) => {
        if (status) {
          changeStatusToActivate(rowData);
        } else {
          changeStatusToDeactivate(rowData);
        }
      },
    },
    {
      Header: "Options",
      accessor: "options",
      type: "menu",
      handlers: {
        edit: (_rowID, row) => {
          setCategory({
            mode: "edit",
            ...row,
          });
          setOpen(true);
        },
        delete: handleDeleteCategory,
      },
    },
  ];

  const actionButtons = [
    {
      caption: "Save",
      onClick: onSubmit,
      // buttonProps: {
      //   disabled: !formState.isValid || category.disableButton,
      // },
    },
    {
      caption: "Clear",
      onClick: category.mode === "edit" ? handleEditClear : handleClear,
    },
  ];

  function handleAddFavoriteButton() {
    setOpen(true);
    setCategory({
      mode: "add",
      name: "",
      description: "",
      category_image: null,
      is_active: true,
    });
    reset();
  }

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    setCategory({
      ...category,
      category_image: file,
    });
  };

  return (
    <Page>
      <Grid container cardspacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
        <Grid item xs={12}>
          <TableWrapper
            apiLoader={loading}
            columns={columns}
            data={data}
            isGlobalFilterEnabled
            tableTitle={<strong>Categories</strong>}
            addOnButton={[handleAddFavoriteButton, "Add Category"]}
          />
        </Grid>
      </Grid>

      {/* Modal for adding/editing Categories */}

      <ModalComponent
        open={open}
        onClose={handleClose}
        title={`${category.mode === "edit" ? "Edit" : "Add"} Categories Group`}
        actionButtons={actionButtons}
        size="medium"
      >
        <Box sx={{ p: 2 }}>
          <form id="myForm" enctype="multipart/form-data">
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <strong>Category Name</strong>
              </Grid>
              <Grid item xs={12} sm={6}>
                <FormControl
                  error={Boolean(errors.card_type)}
                  variant="standard"
                >
                  <Controller
                    name="name"
                    control={control}
                    defaultValue={category.name}
                    onChange={([event]) => event.target.value}
                    render={({ field }) => (
                      <TextField
                        {...field}
                        required
                        id="outlined-required"
                        size="small"
                        style={{ minWidth: 250 }}
                      />
                    )}
                  />
                  <FormHelperText id="component-error-text">
                    {errors.card_type && "* required field"}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={6}>
                <strong>Description</strong>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Controller
                  name="description"
                  control={control}
                  defaultValue={category.description}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      onFocus={(e) => {
                        e.target.select();
                      }}
                      required
                      id="outlined-required"
                      size="small"
                      style={{ minWidth: 250 }}
                    />
                  )}
                />
              </Grid>
              {category.mode === "add" && (
                <>
                  <Grid item xs={12} sm={6}>
                    <strong>Image</strong>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Controller
                      name="category_image"
                      control={control}
                      defaultValue={category.category_image}
                      render={({ field }) => (
                        <TextField
                          {...field}
                          onFocus={(e) => {
                            e.target.select();
                          }}
                          required
                          type="file"
                          accept="image/*"
                          id="outlined-required"
                          size="small"
                          style={{ minWidth: 250 }}
                          onChange={handleFileChange}
                        />
                      )}
                    />
                  </Grid>
                </>
              )}
              <Grid item xs={12} sm={6}>
                <strong>Is Active</strong>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Controller
                  name="is_active"
                  control={control}
                  defaultValue={category.is_active}
                  render={({ field }) => (
                    <Switch
                      {...field}
                      required
                      checked={field.is_active}
                      onChange={(e) => field.onChange(e.target.value)}
                    />
                  )}
                />
              </Grid>
            </Grid>
          </form>
        </Box>
      </ModalComponent>
    </Page>
  );
};

export default Categories;
