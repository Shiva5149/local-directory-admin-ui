import React, { useState } from "react";
import {
  Grid,
  Box,
  TextField,
  FormControl,
  FormHelperText,
  Switch,
} from "@mui/material";
import { useForm, Controller } from "react-hook-form";
import Page from "@assets/layout/Page";
import TableWrapper from "@components/Table/TableWrapper";
import ModalComponent from "@components/Modal";
import useFetch from "@hooks/useFetch";
import apiEndpoints from "@utils/apiEndpoints";
import fetchWrapper from "@utils/fetchWrapper";
import useNotistack from "@hooks/useNotistack";

const TABLE_OPTIONS = [
  { id: "edit", caption: "Edit" },
  { id: "delete", caption: "Delete" },
];

const Users = () => {
  const [data, loading, reFetch] = useFetch(
    apiEndpoints.locations.locationsApi,
    TABLE_OPTIONS
  );
  const [open, setOpen] = useState(false);
  const [location, setLocation] = useState({
    mode: "add",
    address: "",
    city: "",
    state: "",
    country: "",
    latitude: "",
    longitude: "",
    is_active: true,
  });
  const { showNotification } = useNotistack();
  const {
    handleSubmit,
    control,
    formState: { errors },
    reset,
    watch,
  } = useForm();

  const handleClose = () => {
    setOpen(false);
    handleClear();
  };

  const handleClear = () => {
    setLocation({
      mode: "add",
      address: "",
      city: "",
      state: "",
      country: "",
      latitude: "",
      longitude: "",
      is_active: true,
    });
    reset();
  };
  const handleEditClear = () => {
    setLocation({
      mode: "edit",
      address: "",
      city: "",
      state: "",
      country: "",
      latitude: "",
      longitude: "",
      is_active: true,
    });
    reset();
  };

  const handleUpdateLocation = async (data) => {
    try {
      const url = apiEndpoints.locations.locationsApi + `${location.id}/`;
      const response = await fetchWrapper(url, data, "PUT");

      if (response) {
        showNotification("Updated successfully", "success");
        reFetch();
        handleClose();
        handleEditClear();
      } else {
        showNotification("Update failed", "error");
      }
    } catch (error) {
      console.error("Update data error:", error);
    }
  };

  const handleAddLocation = async (data) => {
    try {
      const url = apiEndpoints.locations.locationsApi;
      const response = await fetchWrapper(url, data, "POST");

      if (response) {
        showNotification("Added successfully", "success");
        reFetch();
        handleClose();
        handleClear();
      } else {
        showNotification("Add failed", "error");
      }
    } catch (error) {
      console.error("Add data error:", error);
    }
  };

  const onSubmit = handleSubmit(async (data) => {
    const formData = new FormData();
    formData.append("address", data.address);
    formData.append("city", data.city);
    formData.append("state", data.state);
    formData.append("country", data.country);
    formData.append("latitude", data.latitude);
    formData.append("longitude", data.longitude);

    if (location.mode === "edit") {
      await handleUpdateLocation(formData);
    } else {
      formData.append("is_active", true);
      await handleAddLocation(formData);
    }
  });

  const handleDeleteLocation = async (_rowID, row) => {
    const response = await fetchWrapper(
      apiEndpoints.locations.locationsApi + `${row.id}/`,
      "",
      "DELETE"
    );
    if (response === "success") {
      showNotification(`'${row.address}' Deleted successfully`, "success");
      reFetch();
    }
  };

  const changeStatusToActivate = async (row) => {
    const requestPayload = {
      is_active: !row.is_active,
    };
    const response = await fetchWrapper(
      apiEndpoints.locations.locationsApi + `${row.id}/`,
      requestPayload,
      "PUT"
    );
    if (response) {
      showNotification(
        `'${response.address}' Activated successfully`,
        "success"
      );
      reFetch();
    }
  };

  const changeStatusToDeactivate = async (row) => {
    const requestPayload = {
      is_active: !row.is_active,
    };
    const response = await fetchWrapper(
      apiEndpoints.locations.locationsApi + `${row.id}/`,
      requestPayload,
      "PUT"
    );
    if (response) {
      showNotification(
        `'${response.address}' Deactivated successfully`,
        "success"
      );
      reFetch();
    }
  };

  const columns = [
    {
      Header: "Sl. No",
      accessor: "slno",
    },
    {
      Header: "Address",
      accessor: "address",
    },
    {
      Header: "City",
      accessor: "city",
    },
    {
      Header: "State",
      accessor: "state",
    },
    {
      Header: "Country",
      accessor: "country",
    },
    {
      Header: "Status",
      accessor: "is_active",
      type: "toggle",
      handleToggleChange: (rowData, status) => {
        if (status) {
          changeStatusToActivate(rowData);
        } else {
          changeStatusToDeactivate(rowData);
        }
      },
    },
    {
      Header: "Options",
      accessor: "options",
      type: "menu",
      handlers: {
        edit: (_rowID, row) => {
          setLocation({
            mode: "edit",
            ...row,
          });
          setOpen(true);
        },
        delete: handleDeleteLocation,
      },
    },
  ];

  const actionButtons = [
    {
      caption: "Save",
      onClick: onSubmit,
      // buttonProps: {
      //   disabled: !formState.isValid || location.disableButton,
      // },
    },
    {
      caption: "Clear",
      onClick: location.mode === "edit" ? handleEditClear : handleClear,
    },
  ];

  function handleAddFavoriteButton() {
    setOpen(true);
    setLocation({
      mode: "add",
      address: "",
      city: "",
      state: "",
      country: "",
      latitude: "",
      longitude: "",
      is_active: true,
    });
    reset();
  }
  return (
    <Page>
      <Grid container cardspacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
        <Grid item xs={12}>
          <TableWrapper
            apiLoader={loading}
            columns={columns}
            data={data}
            isGlobalFilterEnabled
            tableTitle={<strong>Locations</strong>}
            addOnButton={[handleAddFavoriteButton, "Add Location"]}
          />
        </Grid>
      </Grid>

      {/* Modal for adding/editing Locations */}

      <ModalComponent
        open={open}
        onClose={handleClose}
        title={`${location.mode === "edit" ? "Edit" : "Add"} Locations Group`}
        actionButtons={actionButtons}
        size="medium"
      >
        <Box sx={{ p: 2 }}>
          <form id="myForm" enctype="multipart/form-data">
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <strong>Address</strong>
              </Grid>
              <Grid item xs={12} sm={6}>
                {/* <FormControl error={errors.address} variant="standard"> */}
                <Controller
                  name="address"
                  control={control}
                  defaultValue={location.address}
                  onChange={([event]) => event.target.value}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      required
                      id="outlined-required"
                      size="small"
                      style={{ minWidth: 250 }}
                    />
                  )}
                />
                {/* <FormHelperText id="component-error-text">
                    {errors.address && "* required field"}
                  </FormHelperText>
                </FormControl> */}
              </Grid>
              <Grid item xs={12} sm={6}>
                <strong>City</strong>
              </Grid>
              <Grid item xs={12} sm={6}>
                {/* <FormControl error={Boolean(errors.city)} variant="standard"> */}
                <Controller
                  name="city"
                  control={control}
                  defaultValue={location.city}
                  onChange={([event]) => event.target.value}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      required
                      id="outlined-required"
                      size="small"
                      style={{ minWidth: 250 }}
                    />
                  )}
                />
                {/* <FormHelperText id="component-error-text">
                    {errors.city && "* required field"}
                  </FormHelperText>
                </FormControl> */}
              </Grid>
              <Grid item xs={12} sm={6}>
                <strong>State</strong>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Controller
                  name="state"
                  control={control}
                  defaultValue={location.state}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      onFocus={(e) => {
                        e.target.select();
                      }}
                      required
                      id="outlined-required"
                      size="small"
                      style={{ minWidth: 250 }}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <strong>Country</strong>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Controller
                  name="country"
                  control={control}
                  defaultValue={location.country}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      onFocus={(e) => {
                        e.target.select();
                      }}
                      required
                      id="outlined-required"
                      size="small"
                      style={{ minWidth: 250 }}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <strong>Latitude</strong>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Controller
                  name="latitude"
                  control={control}
                  defaultValue={location.latitude}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      onFocus={(e) => {
                        e.target.select();
                      }}
                      required
                      id="outlined-required"
                      size="small"
                      style={{ minWidth: 250 }}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <strong>Longitude</strong>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Controller
                  name="longitude"
                  control={control}
                  defaultValue={location.longitude}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      onFocus={(e) => {
                        e.target.select();
                      }}
                      required
                      id="outlined-required"
                      size="small"
                      style={{ minWidth: 250 }}
                    />
                  )}
                />
              </Grid>
            </Grid>
          </form>
        </Box>
      </ModalComponent>
    </Page>
  );
};

export default Users;
