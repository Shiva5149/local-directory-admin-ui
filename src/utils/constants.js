const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
export const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
    },
  },
};

export const ACTIVE = "Active";

export const INACTIVE = "Inactive";

export const CHIPDATAMAPPER = { 1: "Active", 0: "Inactive" };
