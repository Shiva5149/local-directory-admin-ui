import ConfigService from "services/ConfigService";

const { baseURL } = ConfigService.getAppConfig();
console.log("baseURL::", baseURL, process.env);
const endPointMerge = (endpoint) => `${baseURL}${endpoint}`;

const wrapEndpoints = (endpoints) =>
  Object.keys(endpoints).reduce((wrapped, key) => {
    const value = endpoints[key];
    wrapped[key] =
      typeof value === "string" ? endPointMerge(value) : wrapEndpoints(value);
    return wrapped;
  }, {});

export default wrapEndpoints;
