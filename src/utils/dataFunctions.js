/**
 * It takes an array of objects and an id, and returns the object with the matching id, or null if no
 * match is found
 * @param data - the data array
 * @param id - The id of the row you want to find
 * @returns The row data for the row with the given id.
 */
export const findRowData = (data, id) => {
  for (const row of data) {
    if (row.id === id) {
      return row;
    } else if (row.subRows) {
      const subRowData = findRowData(row.subRows, id);
      if (subRowData) {
        return subRowData;
      }
    }
  }
  return null;
};

export const findRowDataCustom = (data, customId, value) => {
  for (const row of data) {
    if (row[customId] === value) {
      return row;
    } else if (row.subRows) {
      const subRowData = findRowData(row.subRows, customId, value);
      if (subRowData) {
        return subRowData;
      }
    }
  }
  return null;
};

/**
 * It takes an array of objects, and recursively updates the `slno` property of each object in the
 * array, and returns the updated array.
 * @param rows - The rows that you want to update.
 * @param startIndex - The starting index of the rows.
 * @returns the updated rows.
 */
export const updateSlnoRecursive = (rows, startIndex = 1) => {
  let slno = startIndex;
  for (let i = 0; i < rows.length; i++) {
    rows[i].id = `${slno}`;
    rows[i].slno = `${slno}`;
    slno++;
    if (rows[i].subRows) {
      rows[i].subRows = updateSlnoRecursive(rows[i].subRows, slno);
      slno += rows[i].subRows.length;
    }
  }
  return rows;
};

/**
 * It takes an array of objects, finds the object with the given id, updates it with the newData, and
 * returns the updated array.
 * @param data - The array of objects that you want to update
 * @param id - The id of the row to be updated
 * @param newData - The new data that you want to update in the array.
 * @param [serialize=false] - If true, it will update the slno of the rows.
 */
export const getUpdatedArray = (data, id, newData, serialize = false) => {
  const updatedData = data.map((row) => {
    if (row.id === id) {
      return { ...row, ...newData };
    } else if (row.subRows) {
      const updatedSubRows = getUpdatedArray(row.subRows, id, newData);
      if (updatedSubRows !== row.subRows) {
        return { ...row, subRows: updatedSubRows };
      }
    }
    return row;
  });

  if (serialize) {
    return updateSlnoRecursive(updatedData, 1);
  }

  return updatedData;
};

/**
 * It adds a new row to the subRows array of the row with the given rowID.
 * @param data - The data array
 * @param rowID - The ID of the row to which you want to add the sub-row.
 * @param rowData - The data to be added to the subRow
 * @param [serialize=false] - If true, it will update the slno of all the rows and subRows.
 */
export const addSubRowData = (data, rowID, rowData, serialize = false) => {
  const addSubRowRecursive = (rows) => {
    for (let i = 0; i < rows.length; i++) {
      const row = rows[i];

      if (row.id === rowID) {
        if (!row.subRows) {
          row.subRows = [];
        }

        row.subRows.push({
          id: data.length + 1,
          slno: row.subRows.length + 1,
          ...rowData,
          options: row.options,
        });

        return true; // Subrow added, exit recursive function
      }

      if (row.subRows && row.subRows.length > 0) {
        const subRowAdded = addSubRowRecursive(row.subRows);
        if (subRowAdded) {
          return true; // Subrow added, exit recursive function
        }
      }
    }

    return false; // Subrow not added
  };

  const subRowAdded = addSubRowRecursive(data);

  if (subRowAdded && serialize) {
    return updateSlnoRecursive(data); // Assuming you have implemented updateSlnoRecursive function separately
  }

  return data;
};

/**
 * It takes an array of objects, finds the index of the object with the given id, removes that object
 * from the array, and returns the new array
 * @param data - The data array
 * @param id - The id of the row to be deleted
 * @param [serialize=false] - If true, the serial number of the rows will be updated.
 * @returns The updated data.
 */
export const deleteRowdata = (data, id, serialize = false) => {
  const deleteRecursive = (rows) => {
    return rows.filter((row) => {
      if (row.id === id) {
        return false; // Remove the row from the data array
      } else if (row.subRows && row.subRows.length > 0) {
        // Recursively search for the row to delete in the sub-rows
        row.subRows = deleteRecursive(row.subRows);
      }
      return true; // Keep the row in the data array
    });
  };

  const newData = deleteRecursive(data);

  if (serialize) {
    return updateSlnoRecursive(newData, 1);
  }

  return newData;
};
