export function checkifNumber(value) {
  return !isNaN(parseFloat(value)) && isFinite(value);
}
