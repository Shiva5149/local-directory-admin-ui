import wrapEndpoints from "@utils/wrapEndpoint";

const apiEndpoints = wrapEndpoints({
  category: {
    categoriesApi: "categories/",
  },
  users: {
    getUsers: "users/",
  },
  locations: {
    locationsApi: "locations/",
  },
  payments: {
    getPayments: "payments/",
  },
  services: {
    getServices: "services/",
  },
  feedback: {
    getFeedback: "feedback/",
  },
});

export default apiEndpoints;
