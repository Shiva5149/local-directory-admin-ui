import axios from "axios";
/**
 * This is an asynchronous function that wraps around the Axios library and adds authorization headers and CORS headers to
 * the request.
 * @param {string} url - The URL of the API endpoint that the function will send a request to.
 * @param {object} [body] - The body parameter is an optional parameter that represents the data to be sent in the
 * request body. It is used when making POST, PUT, and PATCH requests. If it is not provided, the
 * request will not have a body.
 * @param {string} [method="GET"] - The `method` parameter is an optional parameter that specifies the HTTP method to be
 * used for the request. If it is not provided, the default value is "GET".
 * @returns {Promise<any>} - Resolves with the parsed JSON response data if the response status is 200,
 * rejects with a more specific error object containing error information otherwise.
 */
export default async function fetchWrapper(url, body, method = "GET") {
  try {
    const token = localStorage.getItem("token");
    const headers = {
      "Content-Type": "multipart/form-data",
      ...(token && {
        Authorization: `Bearer ${token}`,
      }),
    };

    const options = {
      headers,
    };

    const config = {
      method: method,
      url: url,
      data: body,
      options: options,
    };

    const response = await axios(config);
    const { status, data } = response;
    if (status === 200 || status === 201) {
      return data;
    } else if (status === 204 && method === "DELETE") {
      return "success";
    } else if (status === 401) {
      const error = new Error("Unauthorized");
      error.response = response;
      throw error;
    } else {
      const error = new Error("Request failed");
      error.response = response;
      throw error;
    }
  } catch (error) {
    console.error("Request error:", error);
    throw new Error("Request error");
  }
}
