export const preventDefault = () => {
  document.querySelectorAll("form").forEach((form) => {
    form.addEventListener("submit", (e) => e.preventDefault());
  });
  document.querySelectorAll('a[href="#"]').forEach((a) => {
    a.addEventListener("click", (e) => e.preventDefault());
  });
};

export function generateAlphabet() {
  return [...Array(26)].map((_, i) => String.fromCharCode(i + 97));
}

export const capitalize = (str) => {
  return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
};

export function toggleZeroOne(value) {
  if (value === 0) {
    return 1;
  } else if (value === 1) {
    return 0;
  } else {
    return value;
  }
}

export function camelCaseToCapitalized(str) {
  // Replace camelCase with spaces
  const spacedStr = str.replace(/([A-Z])/g, " $1");
  // Capitalize the first letter and return
  return spacedStr.charAt(0).toUpperCase() + spacedStr.slice(1);
}

export function formatDateTime(date) {
  const options = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
    hour12: true,
  };

  return date.toLocaleString("en-US", options);
}

//Modal Validation
/*
State: Object
Fields: Array
*/
export function Validate(State, Fields) {
  const Status = Fields.filter((item) => {
    if (State[item] === "") return item;
  });

  if (Status.length === 0) return true;
  else {
    const Values = Status.join(", ");
    alert(`Enter ${Values}`);

    return false;
  }
}
