import { useState } from "react";

/**
 * The function returns an object with an index, a setter for the index, and a navigation function that
 * updates the index based on the direction provided.
 * @param array - The `array` parameter is an array of items that the `useArrayNav` hook will navigate
 * through. The hook returns an object with the current index of the array, a function to set the
 * index, and a function to navigate to the next or previous item in the array.
 * @returns An object with three properties: `index`, `setIndex`, and `navigate`.
 */
const useArrayNav = (array) => {
  const [index, setIndex] = useState(0);
  const navigate = (e) => {
    const { direction } = e.currentTarget.dataset;
    if (direction === "next") {
      index + 1 === array.length ? setIndex(0) : setIndex(index + 1);
    } else if (direction === "prev") {
      index - 1 < 0 ? setIndex(array.length - 1) : setIndex(index - 1);
    }
  };

  return {
    index,
    setIndex,
    navigate,
  };
};

export default useArrayNav;
