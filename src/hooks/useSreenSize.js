import { useState, useEffect } from "react";

export default function useScreenSize() {
  const [size, setSize] = useState("");

  useEffect(() => {
    function handleResize() {
      const width = window.innerWidth;
      if (width < 576) {
        setSize("xxs");
      } else if (width < 768) {
        setSize("xs");
      } else if (width < 992) {
        setSize("md");
      } else if (width < 1200) {
        setSize("lg");
      } else {
        setSize("xlg");
      }
    }
    handleResize();
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return size;
}
