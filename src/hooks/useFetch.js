import { useState, useEffect, useCallback } from "react";
import fetchWrapper from "@utils/fetchWrapper";
import useNotistack from "./useNotistack";

export default function useFetch(URL, OPTIONS, ApiStatus) {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [reFetch, setReFetch] = useState(true);
  const { showNotification } = useNotistack();

  const handleReFetch = useCallback(() => {
    setReFetch((prevReFetch) => !prevReFetch);
  }, []);

  async function makeAPICall() {
    setLoading(true);
    setIsError(false);

    function isValidResponse(resData) {
      return Array.isArray(resData);
    }

    function handleInvalidResponse() {
      showNotification("Invalid response", "error");
      setIsError(true);
      setData([]);
    }

    function handleAPIError(error) {
      showNotification(error.message, "error");
      console.log({ error });
      setIsError(true);
      setData([]);
    }

    try {
      const resData = await fetchWrapper(URL);

      if (isValidResponse(resData)) {
        const updatedData = resData.map((eachData, i) => ({
          ...eachData,
          slno: i + 1,
          ...(OPTIONS && { options: OPTIONS }),
        }));
        setData(updatedData);
      } else {
        handleInvalidResponse();
      }
    } catch (error) {
      handleAPIError(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    if (ApiStatus === undefined || ApiStatus === true) {
      makeAPICall();
    }
  }, [reFetch, ApiStatus]);

  return [data, loading, handleReFetch, setData, isError];
}
