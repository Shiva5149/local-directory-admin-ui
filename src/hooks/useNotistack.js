import { useSnackbar } from "notistack";

export default function useNotistack() {
  const { enqueueSnackbar } = useSnackbar();

  const showNotification = (message, variant) => {
    enqueueSnackbar(message, { variant });
  };

  return {
    showNotification,
  };
}