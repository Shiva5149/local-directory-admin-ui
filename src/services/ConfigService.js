const baseURL = process.env.REACT_APP_BACKEND_URL;

const envVars = {
  baseURL,
};

const appConfig = {
  ...envVars,
};

class ConfigService {
  static getAppConfig = () => {
    const appConfigCopy = { ...appConfig };
    Object.freeze(appConfigCopy);
    return appConfigCopy;
  };
}

export default ConfigService;
