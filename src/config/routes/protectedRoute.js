import { Route, Navigate } from "react-router-dom";

const ProtectedRoute = ({ isAuthenticated, ...props }) => {
  if (isAuthenticated) {
    // If authenticated, render the component for the route
    return <Route {...props} />;
  } else {
    // If not authenticated, redirect to the login page
    return <Navigate to="/login" replace />;
  }
};

export default ProtectedRoute;
