import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import ProtectedRoute from "./protectedRoute";
import Counter from "@containers/examples/example";
import NotFoundError from "@containers/pages/errorComponents/404";
import SideMenu from "@components/layout/Sidebar";

export default function AppRoutes() {
  const isAuthenticated = false;
  return (
    <Router>
      <Routes>
        <Route path="/" element={<SideMenu />} />
        <Route
          path="/protected"
          element={
            <ProtectedRoute
              isAuthenticated={isAuthenticated}
              element={<Counter />}
            />
          }
        />
        <Route path="/login" element={<Counter />} />
        <Route path="*" element={<NotFoundError />} />
      </Routes>
    </Router>
  );
}
