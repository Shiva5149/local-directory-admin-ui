import GlobalStyles from "@assets/styles/global";
import "react-grid-layout/css/styles.css";
import "@assets/fonts/icomoon/style.css";
import "swiper/css";
import "swiper/css/effect-fade";
import AppLayout from "./AppLayout";
import { SnackbarProvider } from "notistack";
import { ThemeProvider, StyleSheetManager } from "styled-components";
import {
  ThemeProvider as MuiThemeProvider,
  createTheme,
} from "@mui/material/styles";
import { preventDefault } from "@utils/helpers";
import rtlPlugin from "stylis-plugin-rtl";
import { CacheProvider } from "@emotion/react";
import createCache from "@emotion/cache";
import { SidebarContextAPI } from "@contexts/sidebarContext";

import { useEffect } from "react";
import { useInterfaceContext } from "@contexts/interfaceContext";
import { useDispatch } from "react-redux";
import { saveToLocalStorage } from "@store/features/layout";
import { TableValues } from "@hooks/UseContextData";
import React, { useState } from "react";
import LoginPage from "@pages/Login/LoginPage";

const App = () => {
  const page = document.documentElement;
  const { isDarkMode, isContrastMode, direction } = useInterfaceContext();
  const theme = createTheme({
    direction: direction,
  });
  const cacheRtl = createCache({
    key: "css-rtl",
    stylisPlugins: [rtlPlugin],
  });

  useDispatch()(saveToLocalStorage());

  useEffect(() => {
    page.setAttribute("dir", direction);
  }, [direction]);

  useEffect(() => {
    isContrastMode && page.classList.add("contrast");
    preventDefault();
  }, []);

  //Login Page Logic
  const [Log, setLog] = React.useState(false);

  const handleLogin = (Login) => {
    if (Login === 1) setLog(true);
  };

  let InitialPage = !Log ? <AppLayout /> : <LoginPage sendData={handleLogin} />;
  const [tableRowData, setTableRowData] = useState(null);
  return (
    <CacheProvider value={cacheRtl}>
      <TableValues.Provider value={{ tableRowData, setTableRowData }}>
        <MuiThemeProvider theme={theme}>
          <ThemeProvider theme={{ theme: isDarkMode ? "dark" : "light" }}>
            <SnackbarProvider
              maxSnack={3}
              anchorOrigin={{
                vertical: "top",
                horizontal: direction === "ltr" ? "right" : "left",
              }}
              autoHideDuration={3000}
            >
              <SidebarContextAPI>
                <GlobalStyles />
                <StyleSheetManager
                  stylisPlugins={direction === "rtl" ? [rtlPlugin] : []}
                >
                  <>{InitialPage}</>
                </StyleSheetManager>
              </SidebarContextAPI>
            </SnackbarProvider>
          </ThemeProvider>
        </MuiThemeProvider>
      </TableValues.Provider>
    </CacheProvider>
  );
};

export default App;
