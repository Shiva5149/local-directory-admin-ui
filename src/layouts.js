const layouts = {
  dashboard: {
    lg: [
      { i: "next-patient", x: 0, y: 0, w: 1, h: 1 },
      { i: "laboratory-tests", x: 1, y: 0, w: 1, h: 1 },
      { i: "doctor-upcoming-appointments", x: 2, y: 0, w: 1, h: 4 },
      { i: "doctor-overall-appointment", x: 0, y: 1, w: 1, h: 1 },
      { i: "patients-pace", x: 1, y: 1, w: 1, h: 1 },
      { i: "recent-questions", x: 0, y: 2, w: 1, h: 2 },
      { i: "confirmed-diagnoses", x: 1, y: 2, w: 1, h: 2 },
    ],
    md: [
      { i: "next-patient", x: 0, y: 0, w: 1, h: 1 },
      { i: "laboratory-tests", x: 0, y: 1, w: 1, h: 1 },
      { i: "doctor-upcoming-appointments", x: 1, y: 0, w: 1, h: 4 },
      { i: "doctor-overall-appointment", x: 0, y: 4, w: 1, h: 1 },
      { i: "patients-pace", x: 0, y: 5, w: 1, h: 1 },
      { i: "recent-questions", x: 0, y: 2, w: 1, h: 2 },
      { i: "confirmed-diagnoses", x: 1, y: 4, w: 1, h: 2 },
    ],
  },
  finances: {
    lg: [
      { i: "balance", x: 0, y: 0, w: 1, h: 1 },
      { i: "payments-feed", x: 1, y: 0, w: 2, h: 4 },
      { i: "credit-cards", x: 0, y: 1, w: 1, h: 3 },
    ],
    md: [
      { i: "balance", x: 0, y: 0, w: 1, h: 1 },
      { i: "payments-feed", x: 1, y: 0, w: 1, h: 4 },
      { i: "credit-cards", x: 0, y: 1, w: 1, h: 3 },
    ],
  },
};

export default layouts;
