import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "@redux/reducers/counterSlice";

const store = configureStore({
  reducer: {
    counter: counterReducer,
  },
});

export default store;
