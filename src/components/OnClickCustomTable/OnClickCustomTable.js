import React, { useState, useContext } from "react";
import { useTable } from "react-table";
import {
  Grid,
  Box,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import { TableValues } from "@hooks/UseContextData";

const OnClickCustomTable = ({ data, columns, handleClose }) => {
  const [selectedRowData, setSelectedRowData] = useState(null);
  const { setTableRowData } = useContext(TableValues);

  const useStyles = makeStyles({
    tableRow: {
      "&.selected": {
        backgroundColor: "lightblue",
        cursor: "pointer",
      },
      "&:hover": {
        backgroundColor: "lightgrey",
        cursor: "pointer",
      },
    },
  });

  const classes = useStyles();

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    useTable({
      columns,
      data,
    });

  const handleRowClick = (row) => {
    setSelectedRowData(row.original);
    setTableRowData(row.original);
    handleClose();
  };

  return (
    <>
      <Box>
        <Grid item container xs={12} padding={2} spacing={2} marginBottom={4}>
          <TableContainer component={Paper}>
            <Table {...getTableProps()}>
              <TableHead>
                {headerGroups.map((headerGroup, index) => (
                  <TableRow key={index} {...headerGroup.getHeaderGroupProps()}>
                    {headerGroup.headers.map((column, index) => (
                      <TableCell key={index} {...column.getHeaderProps()}>
                        {column.render("Header")}
                      </TableCell>
                    ))}
                  </TableRow>
                ))}
              </TableHead>
              <TableBody {...getTableBodyProps()}>
                {rows.map((row, i) => {
                  prepareRow(row);
                  return (
                    <TableRow
                      key={i}
                      {...row.getRowProps({
                        className: `${classes.tableRow} ${
                          row.original === selectedRowData ? "selected" : ""
                        }`,
                      })}
                      onClick={() => handleRowClick(row)}
                    >
                      {row.cells.map((cell, index) => {
                        return (
                          <TableCell key={index} {...cell.getCellProps()}>
                            {cell.render("Cell")}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </Box>
    </>
  );
};

export default OnClickCustomTable;
