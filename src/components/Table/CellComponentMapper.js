import React from "react";
import Menu from "@components/Menu";
import SelectComponent from "@components/Select";
import { Chip, Switch } from "@mui/material";
import { makeStyles } from "@mui/styles";
import clsx from "clsx";
import { TextField } from "@mui/material";

const defaultChipDataMapper = {
  1: "Active",
  0: "Inactive",
  2: "open",
  3: "Rejected",
  Cancelled: "Cancelled",
};

const CellMapper = (props) => {
  const { cell, row, column } = props;
  const useStyles = makeStyles(() => ({
    filterDiv: {
      width: "fit-content",
      margin: "0 auto",
    },
    chip: {
      minWidth: "71px",
    },
    Active: {
      backgroundColor: "#82c250",
      color: "white",
    },
    InActive: {
      backgroundColor: "red",
      color: "white",
    },
    New: {
      backgroundColor: "black",
      color: "white",
    },
    hover: {
      cursor: "pointer",
    },
    expanded: {
      paddingLeft: `${row.depth * 2}px`,
    },
    link: {
      color: "rgb(25, 118, 210)",
      cursor: "pointer",
    },
    open: {
      backgroundColor: "blue",
      color: "white",
      cursor: "pointer",
    },
    Rejected: {
      backgroundColor: "red",
      color: "white",
      cursor: "pointer",
    },
    Cancelled: {
      // backgroundColor: "red",
      color: "rgb(25, 118, 210)",
      cursor: "pointer",
    },

    item: {
      color: "rgb(25, 118, 210)",
      cursor: "pointer",
    },
  }));

  const classes = useStyles();
  const type = column?.type;
  const rowID = row?.original?.id;
  const rowData = row.original;
  const handlers = { ...column?.handlers, rowID };
  const data = cell.value;
  const dataMapper = column?.dataMapper ?? defaultChipDataMapper;
  const label = dataMapper[data] ?? data;
  const chipClasses = {
    Active: classes.Active,
    1: classes.Active,
    Inactive: classes.InActive,
    0: classes.InActive,
    InActive: classes.InActive,
    New: classes.New,
    open: classes.open,
    2: classes.open,
    Rejected: classes.Rejected,
    3: classes.Rejected,
    Approved: classes.Approved,
    Cancelled: classes.Cancelled,
  };
  const chipClass = chipClasses[label] ?? chipClasses[data] ?? "";
  if (!data == null) return null;

  switch (type) {
    case "menu":
      return (
        <Menu
          options={data}
          handlers={handlers}
          hideOption={column?.hideOption}
          {...props}
        />
      );
    case "chip":
      return label ? (
        <Chip label={label} className={clsx(classes.chip, chipClass)} />
      ) : null;
    case "dialogLink":
      return (
        <span
          className={classes.link}
          onClick={() => {
            column?.onClick(rowData);
          }}
        >
          {data}
        </span>
      );
    case "Approve":
      return (
        <span
          className={classes.Approved}
          onClick={() => {
            column?.onClick(rowData);
          }}
        >
          {data}
        </span>
      );
    case "expander":
      return (
        <div
          style={{
            minWidth: "130px",
            display: "flex",
            justifyContent: "flex-start",
            marginLeft: "4em",
          }}
        >
          {row.canExpand ? (
            <span
              {...row.getToggleRowExpandedProps({
                title: row.canExpand ? "Expand" : "",
                style: {
                  marginRight: "20px",
                  paddingLeft: `${row.depth * 2}rem`,
                  cursor: row.canExpand ? "pointer" : "unset",
                },
              })}
            >
              {row.isExpanded ? "↧" : "↦"}
            </span>
          ) : null}
          <span
            {...row.getToggleRowExpandedProps({
              title: row.canExpand ? "Expand" : "",
              style: {
                paddingLeft: row.depth > 0 ? `${row.depth * 2}rem` : null,
                cursor: row.canExpand ? "pointer" : "unset",
              },
            })}
          >
            {data}
          </span>
        </div>
      );
    case "select":
      return (
        <SelectComponent
          {...column}
          options={[...column.optionsKey]}
          value={data}
          onChange={(e) => {
            column.onChange(e, rowID, rowData, row);
          }}
        />
      );
    case "textfield":
      return (
        <TextField
          {...column}
          value={data}
          onChange={(e) => {
            column.onChange(e, rowID, rowData, row);
          }}
        />
      );
    case "toggle":
      return (
        <Switch
          checked={data}
          onChange={() => column.handleToggleChange(rowData, !data)}
        />
      );
    case "image":
      return (
        <div style={{ textAlign: "-webkit-center" }}>
          <img src={data} alt={data} style={{ width: "70px" }} />
        </div>
      );
    case "custom":
      return column.Component(rowData, row);

    default:
      return data;
  }
};

export default CellMapper;
