import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import cellMapper from "./CellComponentMapper";

export default function TableComponent({ columns, data }) {
  const useStyles = makeStyles(() => ({
    Active: {
      backgroundColor: "#82c250",
      color: "white",
    },
    InActive: {
      backgroundColor: "red",
      color: "white",
    },
    cellSpacing: {
      padding: "2px",
    },
  }));
  const classes = useStyles();
  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            {columns.map((columnItem) => {
              const headerProps = columnItem?.headerProps ?? {};
              return (
                <TableCell
                  key={columnItem.id}
                  align={"center"}
                  {...headerProps}
                >
                  {columnItem.caption}
                </TableCell>
              );
            })}
          </TableRow>
        </TableHead>
        <TableBody>
          {data?.map((rowItem, index) => {
            const rowID = rowItem?.id;
            const cells = rowItem?.cells;
            const rowProps = rowItem?.rowProps ?? {};
            return (
              <TableRow key={index} {...rowProps}>
                {columns.map((columnItem) => {
                  const data = cells[columnItem?.id];
                  const type = columnItem?.type;
                  const cellProps = columnItem?.cellProps ?? {};
                  const handlers = { ...columnItem?.handlers, rowID };
                  return cellMapper({
                    type,
                    data,
                    handlers,
                    cellProps,
                    classes,
                  });
                })}
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
