import React from "react";
import {
  Paper,
  Table as MaUTable,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Checkbox,
  CircularProgress,
  TablePagination,
} from "@mui/material";
import {
  useTable,
  useFilters,
  useGlobalFilter,
  useSortBy,
  useExpanded,
  useRowSelect,
} from "react-table";
// import { makeStyles } from "@mui/styles";
import { useMemo } from "react";
import GlobalFilter from "@components/Filters/GlobalFilter";

// import { format } from "date-fns";

/**
 * A customizable table component.
 *
 * @component
 * @example
 * const columns = [
 *   { Header: 'Name', accessor: 'name' },
 *   { Header: 'Age', accessor: 'age' },
 *   Add more columns as needed
 * ];
 *
 * const data = [
 *   { name: 'John', age: 25 },
 *   { name: 'Jane', age: 30 },
 *   Add more data rows as needed
 * ];
 *
 * const defaultSelectedRows = {};
 *
 * const setSelectedRows = (selectedRows) => {
 *   Handle selected rows
 * };
 *
 * <Table
 *   columns={columns}
 *   data={data}
 *   isGlobalFilterEnabled={true}
 *   subComponent={CustomSubComponent}
 *   noDataComponent={<span>No data available</span>}
 *   defaultSelectedRows={defaultSelectedRows}
 *   setSelectedRows={setSelectedRows}
 *   isRowSelectionEnabled={true}
 * />
 *
 * @param {Object} props - The component props.
 * @param {Array<Object>} props.columns - The configuration for each column in the table.
 * @param {Array<Object>} props.data - The data rows to be displayed in the table.
 * @param {Array<Object>} props.completeApiData - The complete api data.
 * @param {boolean} [props.isGlobalFilterEnabled=false] - Whether to enable a global filter for the table.
 * @param {function} [props.subComponent] - A custom subcomponent to be rendered for expanded rows.
 * @param {ReactNode} [props.noDataComponent] - A component to be displayed when there is no data available.
 * @param {Object} [props.defaultSelectedRows={}] - The initially selected rows.
 * @param {function} [props.setSelectedRows] - The event handler for updating the selected rows.
 * @param {boolean} [props.isRowSelectionEnabled=false] - Whether to enable row selection in the table.
 * @returns {JSX.Element} The rendered Table component.
 */
function Table({
  columns,
  data,
  isGlobalFilterEnabled,
  subComponent,
  noDataComponent,
  defaultSelectedRows,
  setSelectedRows,
  isRowSelectionEnabled,
  apiLoader,
  tableTitle,
  addOnButton
}) {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [totalPages, setTotalPages] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    const newRowsPerPage = parseInt(event.target.value);
    setRowsPerPage(newRowsPerPage);
    setPage(0);
  };

  const displayPaginationRows = ({ from, to }) => {
    const rowCount = rows ? rows.length : 0;
    return `${from} - ${to} of ${rowCount}`;
  };

  const filterTypes = useMemo(
    () => ({
      text: (rows, id, filterValue) => {
        return rows.filter((row) =>
          row.values[id].toLowerCase().startsWith(filterValue.toLowerCase())
        );
      },
      select: (rows, id, filterValue) =>
        rows.filter((row) => row.values[id] === filterValue),
      date: (rows, id, filterValue) =>
        rows.filter((row) => row.values[id] === filterValue),
      dateBetween: (rows, id, filterValue) => {
        if (!filterValue || (!filterValue.startDate && !filterValue.endDate)) {
          return rows;
        }

        return rows.filter((row) => {
          const value = new Date(row.values[id]);
          const startDate = filterValue.startDate
            ? new Date(filterValue.startDate)
            : null;
          const endDate = filterValue.endDate
            ? new Date(filterValue.endDate)
            : null;

          // If only start date is provided, filter rows that are on or after the start date
          if (startDate && !endDate) {
            return value >= startDate;
          }

          // If only end date is provided, filter rows that are on or before the end date
          if (!startDate && endDate) {
            return value <= endDate;
          }

          // If both start and end dates are provided, filter rows that are between the dates (inclusive)
          return value >= startDate && value <= endDate;
        });
      },
      none: (rows) => rows,
    }),
    []
  );

  const {
    getTableProps,
    headerGroups,
    rows,
    prepareRow,
    state: { globalFilter },
    preGlobalFilteredRows,
    setGlobalFilter,
    toggleAllRowsSelected,
    toggleRowSelected,
    selectedFlatRows,
  } = useTable(
    {
      columns,
      data,
      filterTypes,
      initialState: {
        selectedRowIds: defaultSelectedRows ?? {},
      },
    },
    useGlobalFilter,
    useFilters,
    useSortBy,
    useExpanded,
    useRowSelect,
    (hooks) => {
      if (isRowSelectionEnabled) {
        hooks.visibleColumns.push((columns) => [
          {
            id: "selection",
            Header: ({ getToggleAllRowsSelectedProps }) => (
              <Checkbox
                {...getToggleAllRowsSelectedProps()}
                onChange={(e) =>
                  handleCheckboxChange(e, null, {
                    toggleAllRowsSelected,
                    toggleRowSelected,
                    selectedFlatRows,
                  })
                }
                disableRipple
              />
            ),
            Cell: ({ row }) => (
              <Checkbox
                {...row.getToggleRowSelectedProps()}
                onChange={(e) =>
                  handleCheckboxChange(e, row, {
                    toggleAllRowsSelected,
                    toggleRowSelected,
                    selectedFlatRows,
                  })
                }
                disableRipple
              />
            ),
          },
          ...columns,
        ]);
      }
    }
  );

  const renderRowSubComponent = ({ row }) => {
    const rowData = row.original?.data;
    return subComponent && row.isExpanded
      ? subComponent({ row, rowData })
      : null;
  };

  const handleCheckboxChange = (e, row, tableInstance) => {
    const isChecked = e.target.checked;
    const rowIndex = row ? row.index : -1;
    const isHeaderCheckbox = rowIndex === -1;
    if (isHeaderCheckbox) {
      tableInstance.toggleAllRowsSelected(isChecked);
    } else {
      tableInstance.toggleRowSelected(row.id, isChecked);
    }
    if (isChecked) {
      if (isHeaderCheckbox) {
        setSelectedRows(
          rows.map((row) => ({
            id: row.original.id,
            rowData: row.original,
            row: row,
          }))
        );
      } else {
        setSelectedRows((prev) => {
          return [
            ...prev,
            { id: row.original.id, rowData: row.original, row: row },
          ];
        });
      }
    } else {
      if (isHeaderCheckbox) {
        setSelectedRows([]);
      } else {
        setSelectedRows((prev) =>
          prev.filter((item) => item.id !== row.original.id)
        );
      }
    }
  };

  React.useEffect(() => {
    const newTotalPages = Math.ceil(rows.length / rowsPerPage);
    setTotalPages(newTotalPages);
    setPage((prevPage) => {
      if (prevPage >= newTotalPages) {
        return Math.max(0, newTotalPages - 1);
      }
      return prevPage;
    });
  }, [rows, rowsPerPage]);

  return (
    <TableContainer component={Paper}>
      <MaUTable {...getTableProps()}>
        <TableHead>
          {isGlobalFilterEnabled ? (
            <TableRow>
              <TableCell colSpan={columns.length * 2}>
                <GlobalFilter
                  preGlobalFilteredRows={preGlobalFilteredRows}
                  globalFilter={globalFilter}
                  setGlobalFilter={setGlobalFilter}
                  tableTitle={tableTitle}
                  addOnButton={addOnButton}
                />
              </TableCell>
            </TableRow>
          ) : null}
          {headerGroups.map((headerGroup, index) => {
            const hasFilters = headerGroup.headers.some(
              (column) => column.filterable
            );
            return (
              <React.Fragment key={index}>
                <TableRow {...headerGroup.getHeaderGroupProps()}>
                  {headerGroup.headers.map((column) => (
                    <TableCell
                      key={column?.id}
                      align={"center"}
                      {...column.getHeaderProps(
                        column.sortingEnabled
                          ? column.getSortByToggleProps()
                          : {}
                      )}
                    >
                      {column?.Header ? column.render("Header") : null}
                      {column.sortingEnabled
                        ? column.isSorted
                          ? column.isSortedDesc
                            ? " 🔽"
                            : " 🔼"
                          : ""
                        : null}
                    </TableCell>
                  ))}
                </TableRow>
                {hasFilters && (
                  <TableRow>
                    {headerGroup.headers.map((column) => (
                      <TableCell key={column?.id} align={"center"}>
                        {column.canFilter ? column.render("Filter") : null}
                      </TableCell>
                    ))}
                  </TableRow>
                )}
              </React.Fragment>
            );
          })}
        </TableHead>
        <TableBody>
          {!(rows.length === 0) ? (
            <>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  prepareRow(row);
                  return (
                    <React.Fragment key={row.id}>
                      <TableRow {...row.getRowProps()}>
                        {row.cells.map((cell) => {
                          return (
                            <TableCell
                              key={cell?.id}
                              align={"center"}
                              {...cell.getCellProps()}
                            >
                              {cell.render("Cell")}
                            </TableCell>
                          );
                        })}
                      </TableRow>
                      {renderRowSubComponent({ row, rowData: row.original })}
                    </React.Fragment>
                  );
                })}
              <TablePagination
                rowsPerPageOptions={[10, 25, 50]}
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                nextIconButtonProps={{
                  disabled: page >= totalPages - 1,
                }}
                labelDisplayedRows={displayPaginationRows}
                showFirstButton
                showLastButton
              />
            </>
          ) : (
            <TableRow>
              <TableCell
                align="center"
                styles={{ textAlign: "center" }}
                colSpan={2 * columns.length}
              >
                {apiLoader ? (
                  <CircularProgress />
                ) : noDataComponent ? (
                  <React.Fragment> {noDataComponent} </React.Fragment>
                ) : (
                  <span>No Data Found</span>
                )}
              </TableCell>
            </TableRow>
          )}
        </TableBody>
      </MaUTable>
    </TableContainer>
  );
}

export default Table;
