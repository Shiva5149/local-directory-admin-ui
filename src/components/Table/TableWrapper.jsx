import SearchFilter from "@components/Filters/SearchFilter";
import SelectColumnFilter from "@components/Filters/SelectFilter";
import React from "react";
import CellMapper from "./CellComponentMapper";
import Table from "./ReactTable";
import DateRangeFilter from "@components/Filters/DateRangeFilter";
import DateFilter from "@components/Filters/DateFilter";

const filterTypeMapper = {
  text: SearchFilter,
  select: SelectColumnFilter,
  date: DateFilter,
  dateBetween: DateRangeFilter,
};

// Define the filtering logic for date range filter
function dateRangeFilter(rows, id, filterValue) {
  if (!filterValue || !filterValue.startDate || !filterValue.endDate) {
    return rows;
  }

  // Convert your dates from "dd-mm-yyyy" to "yyyy-mm-dd" format
  const startDate = new Date(
    filterValue.startDate.split("-").reverse().join("-")
  );
  const endDate = new Date(filterValue.endDate.split("-").reverse().join("-"));

  return rows.filter((row) => {
    // Convert row's date from "dd-mm-yyyy" to "yyyy-mm-dd" format
    const rowDate = new Date(row.values[id].split("-").reverse().join("-"));
    return rowDate >= startDate && rowDate <= endDate;
  });
}

function TableWrapper({ columns, ...rest }) {
  const parsedColumns = columns.map((col) => {
    return {
      ...col,
      Cell: (props) => CellMapper(props),
      filterable: col.filterType !== undefined,
      Filter: filterTypeMapper[col.filterType] ?? <></>,
      // Assign the filtering logic to the 'filter' property for 'dateBetween' filter type
      filter: col.filterType === "dateBetween" ? dateRangeFilter : undefined,
    };
  });
  return <Table columns={parsedColumns} {...rest} />;
}

export default TableWrapper;
