import React, { useState } from "react";
import { Grid, Typography, Button } from "@mui/material";
import PropTypes from "prop-types";
import { makeStyles } from "@mui/styles";
import TreeView from "@mui/lab/TreeView";
import TreeItem, { useTreeItem } from "@mui/lab/TreeItem";

import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import GroupsIcon from "@mui/icons-material/Groups";
import ApartmentIcon from "@mui/icons-material/Apartment";
import StarsIcon from "@mui/icons-material/Stars";
import PersonAddDisabledIcon from "@mui/icons-material/PersonAddDisabled";
import PersonAddAlt1Icon from "@mui/icons-material/PersonAddAlt1";
import FolderOpenIcon from "@mui/icons-material/FolderOpen";
import FilePresentIcon from "@mui/icons-material/FilePresent";
import clsx from "clsx";

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
  fontsize: {
    fontSize: 18,
  },
});

function TreeComponent({ nodes, actionButtons }) {
  const classes = useStyles();

  const label = ({ iconType, nodeName }) => {
    const iconMapper = (type) => {
      switch (type) {
        case "groups":
          return <GroupsIcon />;
        case "Office":
          return <ApartmentIcon />;
        case "speciality":
          return <StarsIcon />;
        case "seatnotassigned":
          return <PersonAddDisabledIcon />;
        case "seatassigned":
          return <PersonAddAlt1Icon />;
        case "folder":
          return <FolderOpenIcon />;
        case "file":
          return <FilePresentIcon />;

        default:
          return null;
      }
    };
    return (
      <Grid container padding={1} alignItems={"center"} spacing={2}>
        {iconType && (
          <Grid item marginTop={1}>
            {iconMapper(iconType)}
          </Grid>
        )}
        <Grid item className={classes.fontsize}>
          {nodeName}
        </Grid>
      </Grid>
    );
  };

  const handleMouseEnter = (setIsHovered) => {
    setIsHovered(true);
  };
  const handleMouseLeave = (setIsHovered) => {
    setIsHovered(false);
  };
  const renderActionButtons = (nodeId, pNode) => {
    if (!actionButtons) return null;

    return actionButtons.map((button, index) => {
      if (pNode == undefined && button.caption === "Show Details") {
        return null;
      }

      return (
        <Grid key={index} item>
          <Button
            variant="outlined"
            color={button.caption === "Delete" ? "error" : "primary"}
            onClick={() => button.onClick(nodeId)}
            {...button.buttonProps}
          >
            {button.caption}
          </Button>
        </Grid>
      );
    });
  };

  const CustomContent = React.forwardRef(function CustomContent(props, ref) {
    const {
      classes,
      label,
      nodeId,
      nodeType,
      pNode,
      icon: iconProp,
      expansionIcon,
      displayIcon,
    } = props;
    const {
      disabled,
      expanded,
      selected,
      focused,
      handleExpansion,
      handleSelection,
      preventSelection,
    } = useTreeItem(nodeId);
    const [isHovered, setIsHovered] = useState(false);
    const icon = iconProp || expansionIcon || displayIcon;

    return (
      <div
        className={clsx(classes.root, {
          [classes.expanded]: expanded,
          [classes.selected]: selected,
          [classes.focused]: focused,
          [classes.disabled]: disabled,
        })}
        onMouseDown={preventSelection}
        ref={ref}
      >
        <Grid
          container
          onMouseEnter={() => handleMouseEnter(setIsHovered)}
          onMouseLeave={() => handleMouseLeave(setIsHovered)}
        >
          <Grid item container spacing={1} xs={6} alignItems={"center"}>
            <Grid
              item
              className={classes.iconContainer}
              onClick={handleExpansion}
            >
              {icon}
            </Grid>

            <Grid item>{label}</Grid>
            {nodeType === "file" && (
              <Grid item>
                <Button>Copy</Button>
              </Grid>
            )}
          </Grid>
          {actionButtons && isHovered && (
            <>
              <Grid
                item
                container
                spacing={2}
                alignItems={"center"}
                justifyContent={"end"}
                xs={6}
              >
                {renderActionButtons(nodeId, pNode)}
              </Grid>
            </>
          )}
        </Grid>

        <Typography
          onClick={handleSelection}
          component="div"
          className={classes.label}
        ></Typography>
      </div>
    );
  });

  function renderTree(nodes, pNode) {
    return (
      <TreeItem
        key={nodes.id}
        nodeId={nodes.id}
        label={label({
          nodeName: nodes.name,
          iconType: nodes?.type ?? "",
        })}
        ContentComponent={CustomContent}
        ContentProps={{
          nodeType: nodes?.type,
          pNode: pNode,
        }}
      >
        {Array.isArray(nodes.children)
          ? nodes.children.map((node) => {
              return renderTree(node, node?.id);
            })
          : null}
      </TreeItem>
    );
  }
  return (
    <div className={classes.root}>
      <TreeView
        defaultCollapseIcon={<ExpandMoreIcon />}
        defaultExpandIcon={<ChevronRightIcon />}
      >
        {renderTree(nodes)}
      </TreeView>
    </div>
  );
}

TreeComponent.propTypes = {
  nodes: PropTypes.object.isRequired,
};

export default TreeComponent;
