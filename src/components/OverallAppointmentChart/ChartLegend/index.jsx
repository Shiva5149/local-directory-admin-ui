import LegendItem from "@assets/UI/Legend/LegendItem";
import Legend from "@assets/UI/Legend";
import { colorTypes } from "@assets/constants/colors";

const ChartLegend = () => {
  return (
    <Legend>
      {colorTypes.map(({ cat, color }) => {
        if (cat === "emergency") {
          return null;
        } else {
          return <LegendItem key={cat} legend={cat} color={color} />;
        }
      })}
    </Legend>
  );
};

export default ChartLegend;
