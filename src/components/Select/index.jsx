import { MenuItem, Select } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { MenuProps } from "@utils/constants";
import React from "react";

const useStyles = makeStyles(() => ({
  root: {
    minWidth: "10ch",
  },
}));

/**
 * A customizable select component.
 *
 * @component
 * @example
 * Example 1: Options as an array of strings
 * const options1 = ['option1', 'option2'];
 * const selectedValue1 = 'option1';
 *
 * const handleChange1 = (event) => {
 *   setSelectedValue1(event.target.value);
 * };
 *
 * <SelectComponent options={options1} value={selectedValue1} onChange={handleChange1} />
 *
 * Example 2: Options as an array of objects
 * const options2 = [
 *   { label: 'Option 1', value: 'option1' },
 *   { label: 'Option 2', value: 'option2' },
 * ];
 * const selectedValue2 = 'option1';
 *
 * const handleChange2 = (event) => {
 *   setSelectedValue2(event.target.value);
 * };
 *
 * <SelectComponent options={options2} value={selectedValue2} onChange={handleChange2} />
 *
 * @param {Object} props - The component props.
 * @param {string} props.value - The currently selected value.
 * @param {function} props.onChange - The event handler for value change.
 * @param {boolean} [props.fullWidth=false] - Whether the component should take full width.
 * @param {Array|string[]} [props.options=[]] - The options to be displayed in the select.
 * @param {string} [props.id=""] - The HTML id attribute of the select element.
 * @param {string} [props.name=""] - The HTML name attribute of the select element.
 * @param {string} [props.size="small"] - The size of the select component.
 * @param {Object} [props.menuItemProps={}] - Additional props to be passed to each MenuItem component.
 * @param {Object} [props.labelMapper] - An object mapping values to custom labels.
 * @param {'...'} [props.rest] - Additional props to be passed to the Select component.
 * @returns {JSX.Element} The rendered SelectComponent.
 */
function SelectComponent({
  value,
  onChange,
  fullWidth = false,
  options = [],
  id = "",
  name = "",
  size = "small",
  menuItemProps = {},
  ...rest
}) {
  const classes = useStyles();

  const transformedOptions = options.map((item) => {
    if (typeof item === "string") {
      return { label: item, value: item };
    }
    return item;
  });

  return (
    <div className={classes.root}>
      <Select
        id={id}
        name={name}
        value={value === "" ? transformedOptions[0].value : value}
        onChange={onChange}
        fullWidth={fullWidth}
        size={size}
        MenuProps={MenuProps}
        {...rest}
      >
        {transformedOptions.map((item) => (
          <MenuItem key={item.value} value={item.value} {...menuItemProps}>
            {item.label}
          </MenuItem>
        ))}
      </Select>
    </div>
  );
}

export default SelectComponent;
