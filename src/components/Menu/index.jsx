import * as React from "react";
import IconButton from "@mui/material/IconButton";
import MuiMenu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import { Divider } from "@mui/material";

export default function Menu({ options, handlers, row, hideOption }) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const rowData = row.original;
  return (
    <div>
      <IconButton
        aria-label="more"
        id="long-button"
        aria-controls={open ? "long-menu" : undefined}
        aria-expanded={open ? "true" : undefined}
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MoreVertIcon />
      </IconButton>
      <MuiMenu
        id="long-menu"
        aria-labelledby="long-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
      >
        {options.map((option, index) => {
          if (hideOption && hideOption(rowData, option)) return null;
          const handleOnClick = handlers?.[option.id];
          return (
            <div key={option.caption}>
              {index === 0 ? null : <Divider />}
              <MenuItem
                key={option.caption}
                onClick={() => {
                  handleClose();
                  handleOnClick(handlers?.rowID, rowData, row);
                }}
              >
                {option.caption}
              </MenuItem>
            </div>
          );
        })}
      </MuiMenu>
    </div>
  );
}
