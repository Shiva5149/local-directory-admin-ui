import React from "react";

import { TextField } from "@mui/material";

function DateFilter({ column: { filterValue, setFilter } }) {
  // Ensure date is in the dd-mm-yyyy format
  const formatDate = (dateString) => {
    if (dateString) {
      const parts = dateString.split("-");
      return `${parts[2]}-${parts[1]}-${parts[0]}`;
    }

    return "";
  };

  const formatBack = (dateString) => {
    if (dateString) {
      const parts = dateString.split("-");
      return `${parts[2]}-${parts[1]}-${parts[0]}`;
    }
    return undefined;
  };

  return (
    <TextField
      size="small"
      title="Start Date"
      type="date"
      value={formatDate(filterValue ?? "")}
      onChange={(e) => setFilter(formatBack(e.target.value))}
    />
  );
}

export default DateFilter;
