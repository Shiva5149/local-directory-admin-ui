import { MenuItem, Select } from "@mui/material";
import React from "react";

export default function SelectColumnFilter({
  column: { filterValue, setFilter, preFilteredRows, id, dataMapper },
}) {
  const options = React.useMemo(() => {
    const options = new Set();
    preFilteredRows.forEach((row) => {
      options.add(row.values[id]);
    });
    return [...options.values()];
  }, [id, preFilteredRows]);

  return (
    <Select
      size="small"
      value={filterValue}
      onChange={(e) => {
        setFilter(e.target.value || undefined);
      }}
      style={{ minWidth: "100px" }}
    >
      <MenuItem value="">All</MenuItem>
      {options.map((option, i) => {
        const optionToString = option?.toString();
        return (
          <MenuItem key={i} value={optionToString}>
            {dataMapper?.[optionToString] ?? option}
          </MenuItem>
        );
      })}
    </Select>
  );
}
