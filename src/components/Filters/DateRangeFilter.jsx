import React from "react";
import { Grid, TextField } from "@mui/material";

export default function DateRangeFilter({ column: { setFilter } }) {
  const handleStartDateChange = (e) => {
    const startDate = e.target.value;
    setFilter((prevFilter) => ({
      ...prevFilter,
      startDate: startDate ? startDate.split("-").reverse().join("-") : null,
    }));
  };

  const handleEndDateChange = (e) => {
    const endDate = e.target.value;
    setFilter((prevFilter) => ({
      ...prevFilter,
      endDate: endDate ? endDate.split("-").reverse().join("-") : null,
    }));
  };

  return (
    <Grid container justifyContent={"center"} spacing={1}>
      <Grid item>
        <TextField
          size="small"
          title="Start Date"
          type="date"
          onChange={handleStartDateChange}
        />
      </Grid>
      <Grid item>
        <TextField
          size="small"
          title="End Date"
          type="date"
          onChange={handleEndDateChange}
        />
      </Grid>
    </Grid>
  );
}
