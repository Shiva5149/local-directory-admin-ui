import { TextField, Button } from "@mui/material";
import { makeStyles } from "@mui/styles";
import React from "react";
import { useAsyncDebounce } from "react-table";

export default function GlobalFilter({
  globalFilter,
  setGlobalFilter,
  tableTitle,
  addOnButton,
}) {
  const useStyles = makeStyles(() => ({
    flex: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
    },
    margin: {
      marginRight: "10px",
    },
    title: {
      fontSize: "large",
    },
    textButtonContainer: {
      display: "flex",
      justifyContent: "space-between",
    },
  }));

  const classes = useStyles();

  const [value, setValue] = React.useState(globalFilter);
  const onChange = useAsyncDebounce((value) => {
    setGlobalFilter(value || undefined);
  }, 200);

  const [handleOnClick, buttonTitle] = addOnButton ?? [];

  return (
    <div className={classes.flex}>
      <div className={classes.title}>{tableTitle && tableTitle}</div>
      <div className={classes.textButtonContainer}>
        <TextField
          size="small"
          value={value || ""}
          onChange={(e) => {
            setValue(e.target.value);
            onChange(e.target.value);
          }}
          style={{ minWidth: "100px", marginRight: addOnButton ? "8px" : "0" }}
          placeholder={`Search...`}
        />
        {addOnButton && (
          <Button color="primary" variant="outlined" onClick={handleOnClick}>
            {buttonTitle}
          </Button>
        )}
      </div>
    </div>
  );
}
