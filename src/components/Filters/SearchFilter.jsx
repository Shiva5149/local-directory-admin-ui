import { TextField } from "@mui/material";

export default function SearchFilter({ column: { filterValue, setFilter } }) {
  return (
    <TextField
      size="small"
      value={filterValue || ""}
      onChange={(e) => {
        setFilter(e.target.value || undefined); // Set undefined to remove the filter entirely
      }}
      style={{ maxWidth: "13em" }}
      placeholder={``}
    />
  );
}
