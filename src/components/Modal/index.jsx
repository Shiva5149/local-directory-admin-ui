import React from "react";
import { makeStyles } from "@mui/styles";
import CloseIcon from "@mui/icons-material/Close";
import { Button, Grid, Typography, Modal, Chip } from "@mui/material";
import { useEffect } from "react";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4),
    position: "relative",
    outline: "none",
    borderRadius: "5px",
    maxHeight: "calc(100% - 64px)",
    display: "flex",
    flexDirection: "column",
    margin: "32px",
    overflow: "auto",
  },
  header: {
    marginBottom: "10px",
    paddingBottom: "10px",
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  title: {
    fontSize: "20px",
    lineHeight: "24px",
    alignItems: "center",
  },
  subTitle: {},
  closeIcon: {
    borderRadius: "50%",
    color: "black",
    right: "-20px",
    "&:hover": {
      backgroundColor: "transparent !important",
    },
  },
  footer: {
    marginTop: "10px",
    paddingTop: "10px",
    borderTop: `1px solid ${theme.palette.divider}`,
  },
  small: {
    width: "500px",
  },
  medium: {
    width: "700px",
  },
  large: {
    width: "900px",
  },
  extralarge: {
    width: "1000px",
  },
  doubleExtralarge: {
    width: "1200px",
  },
}));

/**
 * ModalComponent is a reusable component that displays a modal dialog.
 *
 * @component
 * @param {Object} props - The component props.
 * @param {boolean} props.open - Determines whether the modal is open or closed.
 * @param {function} props.onClose - The function to be called when the modal is closed.
 * @param {string} props.title - The title of the modal.
 * @param {ReactNode} props.content - The content to be displayed in the modal.
 * @param {ReactNode} props.header - The custom header to be displayed in the modal.
 * @param {ReactNode} props.footer - The custom footer to be displayed in the modal.
 * @param {Array} props.actionButtons - An array of action buttons to be displayed in the modal footer.
 * @param {string} props.size - The size of the modal. Possible values: 'small', 'medium', 'large', 'extralarge', 'doubleExtralarge'.
 * @param {ReactNode} props.children - Additional children to be rendered within the modal.
 * @returns {JSX.Element} The rendered ModalComponent.
 */
const ModalComponent = ({
  open,
  onClose,
  title,
  content,
  header,
  footer,
  actionButtons,
  size,
  children,
  subTitle,
}) => {
  const classes = useStyles();
  const html = document.documentElement;

  const renderActionButtons = () => {
    if (!actionButtons) return null;

    return actionButtons.map((button, index) => (
      <Grid key={index} item marginRight={2}>
        <Button
          variant="outlined"
          onClick={button.onClick}
          //set the buttom type is submit

          {...button.buttonProps}
        >
          {button.caption}
        </Button>
      </Grid>
    ));
  };

  const renderHeader = () => {
    if (header) return header;
    return (
      <Grid
        container
        justifyContent="space-between"
        alignItems="center"
        className={classes.header}
      >
        <Grid item>
          <Typography
            variant="h6"
            component="h2"
            className={classes.title}
            id="modal-title"
            spacing={1}
            direction="row"
          >
            <span>
              {title}
              {subTitle && ":"}
            </span>
            {subTitle && (
              <Chip
                className={classes.subTitle}
                label={subTitle}
                variant="outlined"
              />
            )}
          </Typography>
        </Grid>
        <Grid item>
          <Button
            size="small"
            disableRipple
            className={classes.closeIcon}
            onClick={onClose}
          >
            <CloseIcon />
          </Button>
        </Grid>
      </Grid>
    );
  };

  const renderFooter = () => {
    return (
      footer ??
      (actionButtons ? (
        <Grid
          container
          justifyContent="center"
          alignItems="center"
          className={classes.footer}
        >
          {renderActionButtons()}
        </Grid>
      ) : null)
    );
  };

  const sizeClasses = {
    small: classes.small,
    sm: classes.small,
    medium: classes.medium,
    md: classes.medium,
    lg: classes.large,
    large: classes.large,
    xl: classes.extralarge,
    extralarge: classes.extralarge,
    doubleExtralarge: classes.doubleExtralarge,
    xxl: classes.doubleExtralarge,
  };

  const sizeClass = sizeClasses[size] ?? size ?? "";

  useEffect(() => {
    open ? html.classList.add("no-scroll") : html.classList.remove("no-scroll");
  }, [open]);

  return (
    <Modal
      open={open}
      onClose={onClose}
      scroll={"paper"}
      className={classes.modal}
      aria-labelledby="modal-title"
      aria-describedby="modal-description"
    >
      <div className={`${classes.paper} ${sizeClass}`}>
        {renderHeader()}
        <div>{content ?? children ?? ""}</div>

        {renderFooter()}
      </div>
    </Modal>
  );
};

export default ModalComponent;
