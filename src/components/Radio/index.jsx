import { FormControlLabel, Radio, RadioGroup } from "@mui/material";
import React from "react";

/**
 * A customizable radio group component.
 *
 * @component
 * @example
 * Example 1: Options as an array of strings
 * const options1 = ['yes', 'no'];
 * const selectedValue1 = 'yes';
 *
 * const handleChange1 = (event) => {
 *   setSelectedValue1(event.target.value);
 * };
 *
 * <RadioComponent options={options1} value={selectedValue1} onChange={handleChange1} />
 *
 * Example 2: Options as an array of objects
 * const options2 = [
 *   { label: 'Yes', value: 1 },
 *   { label: 'No', value: 0 },
 * ];
 * const selectedValue2 = 1;
 *
 * const handleChange2 = (event) => {
 *   setSelectedValue2(event.target.value);
 * };
 *
 * <RadioComponent options={options2} value={selectedValue2} onChange={handleChange2} />
 *
 * @param {Object} props - The component props.
 * @param {Array|string[]} props.options - The options to be displayed as radio buttons.
 * @param {string} props.value - The currently selected value.
 * @param {function} props.onChange - The event handler for value change.
 * @param {boolean} [props.row=true] - Whether the radio buttons should be displayed in a row.
 * @param {'...'} [props.rest] - Additional props to be passed to the RadioGroup component.
 * @returns {JSX.Element} The rendered RadioComponent.
 */
function RadioComponent({ options, value, onChange, row = true, ...rest }) {
  const transformedOptions = options.map((item) => {
    if (typeof item === "string") {
      return { label: item, value: item };
    }
    return item;
  });

  return (
    <RadioGroup value={value} onChange={onChange} row={row} {...rest}>
      {transformedOptions.map((item) => {
        return (
          <FormControlLabel
            key={item.label}
            value={item.value}
            control={<Radio />}
            label={item.label}
          />
        );
      })}
    </RadioGroup>
  );
}

export default RadioComponent;
