import React, { useState } from "react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";

const TextEditor = ({ data = "", setData }) => {
  const [editorValue, setEditorValue] = useState(data);

  const modules = {
    toolbar: [
      ["bold", "italic", "underline", "strike"],
      ["link", "blockquote", "code-block"],
      [{ header: 1 }, { header: 2 }],
      [({ list: "ordered" }, { list: "bullet" })],
      [{ script: "sub" }, { script: "super" }],
      [{ indent: "-1" }, { indent: "+1" }],
      [{ direction: "rtl" }],
      [{ size: ["small", false, "large", "huge"] }],
      ["link", "image", "video"],
      [{ color: [] }, { background: [] }],
      [{ font: [] }],
      [{ align: [] }],
      ["clean"],
    ],
  };

  const handleChange = (newValue) => {
    setEditorValue(newValue);
    setData && setData(newValue);
  };

  return (
    <ReactQuill
      theme="snow"
      value={editorValue}
      modules={modules}
      onChange={handleChange}
    />
  );
};

export default TextEditor;
