import * as React from "react";
import { styled } from "@mui/material/styles";
import MuiAccordion from "@mui/material/Accordion";
import MuiAccordionSummary from "@mui/material/AccordionSummary";
import MuiAccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import KeyboardDoubleArrowRightIcon from "@mui/icons-material/KeyboardDoubleArrowRight";
import { makeStyles } from "@mui/styles";
import { createTheme, ThemeProvider } from "@mui/material/styles";

const useStyles = makeStyles(() => ({
  accordionSummary: {
    color: "red",
    backgroundColor: "red",
    "& ..MuiButtonBase-root": {
      backgroundColor: "red !important",
    },
  },
}));

const StyledAccordion = styled((props) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  border: `1px solid ${theme.palette.divider}`,
  "&:not(:last-child)": {
    borderBottom: 0,
  },
  "&:before": {
    display: "none",
  },
}));

const theme = createTheme({
  components: {
    MuiPaper: {
      styleOverrides: {
        root: {
          "& .MuiButtonBase-root": {
            "&:hover": {
              backgroundColor: "white !important",
              color: "#2662F0 !important",
            },

            "&:focus": {
              backgroundColor: "white !important",
              color: "black !important",
            },
          },
        },
      },
    },
  },
});

const AccordionSummary = styled((props) => (
  <MuiAccordionSummary
    expandIcon={<KeyboardDoubleArrowRightIcon sx={{ fontSize: "0.9rem" }} />}
    {...props}
  />
))(({ theme }) => ({
  backgroundColor:
    theme.palette.mode === "dark"
      ? "rgba(255, 255, 255, .05)"
      : "rgba(0, 0, 0, .03)",
  flexDirection: "row-reverse",
  "& .MuiAccordionSummary-expandIconWrapper.Mui-expanded": {
    transform: "rotate(90deg)",
  },
  "& .MuiAccordionSummary-content": {
    marginLeft: theme.spacing(1),
    justifyContent: "space-between",
    display: "flex",
  },
}));

const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
  padding: theme.spacing(2),
  borderTop: "1px solid rgba(0, 0, 0, .125)",
}));

export default function Accordion({ bodyContent, buttonContent, header }) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState("panel1");

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  return (
    <ThemeProvider theme={theme}>
      <div>
        <StyledAccordion
          expanded={expanded === "panel1"}
          onChange={handleChange("panel1")}
        >
          <AccordionSummary
            className={classes.accordionSummary}
            style={{ backgroundColor: "red !important" }}
          >
            <Typography>{header}</Typography>
            <div style={{ display: "flex" }}>{buttonContent}</div>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>{bodyContent}</Typography>
          </AccordionDetails>
        </StyledAccordion>
      </div>
    </ThemeProvider>
  );
}
