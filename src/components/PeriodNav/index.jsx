import { Container, Item, Button } from "@assets/UI/TabNav/style";
import usePeriodNav from "@hooks/usePeriodNav";
import { nanoid } from "nanoid";
const PeriodNav = ({ current, handler }) => {
  const { periods } = usePeriodNav();

  return (
    <Container>
      {periods.map((p) => (
        <Item key={nanoid(4)}>
          <Button
            className={current === p && "active"}
            onClick={() => handler(p)}
          >
            {p}
          </Button>
        </Item>
      ))}
    </Container>
  );
};

export default PeriodNav;
