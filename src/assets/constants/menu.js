import PeopleIcon from '@mui/icons-material/People';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import CategoryIcon from '@mui/icons-material/Category';
import MiscellaneousServicesIcon from '@mui/icons-material/MiscellaneousServices';
import ReceiptIcon from '@mui/icons-material/Receipt';
import RateReviewIcon from '@mui/icons-material/RateReview';

export const menu = [
  {
    name: "Users",
    icon: PeopleIcon,
    link: "/users",
  },
  {
    name: "Locations",
    icon: LocationOnIcon,
    link: "/locations",
  },
  {
    name: "Categories",
    icon: CategoryIcon,
    link: "/categories",
  },
  {
    name: "Services",
    icon: MiscellaneousServicesIcon,
    link: "/services",
  },
  {
    name: "Payments",
    icon: ReceiptIcon,
    link: "/payments",
  },
  {
    name: "Feedback",
    icon: RateReviewIcon,
    link: "/feedback",
  },
];
