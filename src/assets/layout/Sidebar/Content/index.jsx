import { List, MainItem } from "../style";
import { colors } from "@assets/styles/vars";
import Accordion from "react-bootstrap/Accordion";
import { NavLink } from "react-router-dom";
import { useSidebarContext } from "@contexts/sidebarContext";
import { menu } from "@assets/constants/menu";

const Content = () => {
  const { toggleSidebar } = useSidebarContext();
  const activeStyle = { color: colors.blue };

  return (
    <List as={Accordion}>
      {menu.map((item, index) => {
        if (item.link) {
          return (
            <MainItem
              as={NavLink}
              to={item.link}
              onClick={() => toggleSidebar()}
              style={({ isActive }) => (isActive ? activeStyle : undefined)}
              key={item.link}
              className={index === menu.length - 1 ? "pin-down" : ""}
            >
              <item.icon /> {item.name}
            </MainItem>
          );
        } else return null;
      })}
    </List>
  );
};

export default Content;
