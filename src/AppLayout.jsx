import { lazy, Suspense } from "react";
import React from "react";
import Panel from "@assets/layout/Panel";
import Sidebar from "@assets/layout/Sidebar";
import { Navigate, Route, Routes } from "react-router-dom";
import WidgetsLoader from "@components/WidgetsLoader";
import { useRef, useEffect } from "react";

const Dashboard = lazy(() => import("@pages/Dashboard"));
const PageNotFound = lazy(() => import("@components/PageNotFound"));
const Categories = lazy(() => import("@pages/Categories"));
const Locations = lazy(() => import("@pages/Locations"));
const Users = lazy(() => import("@pages/Users"));
const Services = lazy(() => import("@pages/Services"));
const Payments = lazy(() => import("@pages/Payments"));
const Feedback = lazy(() => import("@pages/Feedback"));

const routes = [
  { path: "/categories", element: <Categories /> },
  { path: "/locations", element: <Locations /> },
  { path: "/users", element: <Users /> },
  { path: "/services", element: <Services /> },
  { path: "/payments", element: <Payments /> },
  { path: "/feedback", element: <Feedback /> },
  { path: "/", element: <Navigate to="/dashboard" /> },
  { path: "/dashboard", element: <Dashboard /> },
  { path: "/404", element: <PageNotFound /> },
];

const AppLayout = () => {
  const appRef = useRef(null);

  useEffect(() => {
    if (appRef.current) {
      appRef.current.scrollTo(0, 0);
    }
  }, []);

  return (
    <div className="app" ref={appRef}>
      <Sidebar />

      <div className="app_content">
        <Panel />
        <Suspense fallback={<WidgetsLoader />}>
          <Routes>
            {routes.map((prop, key) => {
              return (
                <Route key={key} path={prop.path} element={prop.element} />
              );
            })}
            <Route path="*" element={<Navigate to="/404" replace />} />
          </Routes>
        </Suspense>
      </div>
    </div>
  );
};

export default AppLayout;
