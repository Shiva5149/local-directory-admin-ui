import { configureStore } from "@reduxjs/toolkit";
import LayoutsReducer from "./features/layout";
import TodosReducer from "./features/todos";
import CardsReducer from "./features/cards";

const store = configureStore({
  reducer: {
    layout: LayoutsReducer,
    todos: TodosReducer,
    cards: CardsReducer,
  },
});

export default store;
